
This is the SSITH CWE Attacks Suite.

OVERVIEW
========

This attack suite performs end-to-end attacks using multi-security-domain based penetration attacks. The attacks fall into the 7 SSITH CWE attack classes, as follows:

(1) Buffer Errors:

exploit-smash.c, CWE-121: Stack-based Buffer Overflow

In this end-to-end attack, a buffer overflow vulnerability is used to write a malicious code pointer into a neighboring function pointer table. If successful, the attack overtakes a function pointer, redirecting it to a malicious code function. This particular attack scenario was adapted from the existing Testgen buffer overflow vulnerability test; we enhanced it to inject a code pointer into a neighboring function table.

(2) Permission, Privileges and Access Control:

exploit-perms.c, CWE-287: Improper Authentication

In this attack scenario, the vulnerable program maintains a permissions level, which is a data pointer to a structure variable that details the current permissions capabilities. During the end-to-end attack, a too-long username is passed to a username validation function, which results in a buffer overwrite exploit in to the neighboring permissions level variable. Once the permissions variable is overwritten, the user can assume high-level permission operation despite not authenticating properly with the vulnerable program.

(3) Resource Management:

exploit-UAF.c, CWE-416: Use After Free

In this end-to-end attack, a use-after-free of a dynamic storage allocation is exploited to penetrate the vulnerable program. A data structure with code pointers is built, used and then freed, but the vulnerable program keeps an pointer to the freed structure. Later, the program, via external input, allocates the same block of storage and stores the integer input into the same location as the previous allocation’s code pointer. Then a bug in the program is exploited to dereference a dangling code pointer to the freed allocation, resulting in a successful control flow attack.

(4) Code Injection:

exploit-inject.c, CWE-94: Improper Control of Generation of Code (‘Code Injection’)

In this end-to-end attack, the attacker injects trampoline code into a stack buffer of a vulnerable program. The trampoline code is a single jump instruction, which conveniently allows the attacker to jump to any location in the program. It is injected onto the stack using a string copy buffer overflow. As such, it is required that the injected code contain no zero bytes (as is the case in the example trampoline code snippet). After injecting the trampoline code, the return address is overwritten to jump to the trampoline. On return from the memory copy, the program jumps to the trampoline code, which then jumps to an attacker-selected code entry point.

(5) Information Leakage (also known as Information Exposure)

exploit-leak.c, CWE-209: Information Exposure Through an Error Message

In this end-to-end attack scenario, an attempted buffer overflow fails due to a properly formed array buffer access check. However, when the errant input is detected by the program, it unwisely prints the offending address in an error and continues to execute. This results in a successful stack de-randomization attack, as now the exact location of the running program’s stack is now known to the attacker.

(6) Crypto Errors

exploit-scrape.c, CWE-257: Storing Passwords in a Recoverable Format

In this end-to-end attack, a malicious entity attempts to recover a plaintext password stored in heap memory by searching for an identifying pattern. A data structure storing login credentials is allocated on the heap. Usernames and passwords are stored as plaintext, but passwords are flagged as sensitive (so that potential defenses can be applied to this valuable data). In addition, passwords are bookended by a cookie (e.g., #!). After the credentials are initialized, an attacker sequentially reads memory to locate the cookie and the subsequent password. This password is used for a login attempt, which succeeds.

(7) Numeric Errors

eploit-overflow.c, CWE-190: Integer Overflow or Wraparound

In this end-to-end attack, a string in the input to the vulnerable code initiates an overflow of a buffer that is sufficiently long that it’s length computation overflows a byte integer check to see if the string is too long. The overly long string check passes, due to a byte-integer overflow, and then a stack smashing attack is initiated. A return address is injected into the stack and the attacker penetrates the program.

PORTING AND BUILDING
====================

Like all end-to-end security attacks, they are generally fragile and need to be ported to your environment before they will work on the unprotected machine. The attacks assume currently a 32-bit architecture, and they also assume the underlying architecture is a RISC-V based machine. Note that small changes to the code, or even small changes to the compiler, can result in an exploit breaking in a way that it no longer functions properly.

To port the attacks to your build environment, please see the "PORTING DETAILS" comments in the exploit-*.c files.

Good luck, may the FORCE be with you!

AUTHORS:

Lauren Biernacki <LBIERNAC@umich.edu>
Austin Harris <austinharris@utexas.edu>
Todd Austin <austin@umich.edu>


