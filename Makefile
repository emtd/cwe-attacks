# Link with gcc always
GCC = gcc
ifeq ($(BUILD_CLANG), )
CC = $(GCC) $(CFLAGS)
OBJDUMP = riscv32-unknown-elf-objdump
CCOPTS = -g -O3 
else
CLANG_OPTS = -ffixed-x5 --target=riscv32-unknown-elf -c -Xclang -load -Xclang $(EMTD_PREFIX)/lib/MorpheusPointerEncryption.so -g -mllvm -encrypt --sysroot=/home/kkulat/agita/gfe_old/riscv-tools/riscv32-unknown-elf
CCOPTS = -mcmodel=medany -mabi=ilp32 -march=rv32im -g -O2 -fno-inline-functions -ffast-math -fno-common -fno-builtin-printf -nostdlib -nodefaultlibs -fno-inline-functions -fno-builtin $(CLANG_OPTS)
OBJDUMP = llvm-objdump
CC = clang $(CFLAGS)
endif

LDOPTS =

ENCRYPTOR = ../elf-parser/elf-parser
INCS = -I.
EMU = $(RISCV)/../emulator/emulator-freechips.rocketchip.system-DefaultConfig
DASM = $(RISCV)../riscv-toolchain/bin/spike-dasm

ATTACKS = exploit-inject exploit-leak exploit-overflow exploit-perms exploit-smash exploit-UAF exploit-scrape

all: $(ATTACKS)

tests:
	./exploit-inject
	./exploit-leak
	./exploit-overflow
	./exploit-perms
	./exploit-smash
	./exploit-UAF
	./exploit-scrape

exploit-inject: exploit-inject.o
	$(GCC) $(INCS) -o $@ $^ $(LDOPTS)

crt.o: crt.S
	$(CC) $(INCS) $(CCOPTS) -c crt.S

syscalls.o: syscalls.c util.h
	$(CC) $(INCS) $(CCOPTS) -c syscalls.c

cwe-257.o: cwe-257-passwords.c
	$(CC) $(INCS) $(CCOPTS) -c cwe-257-passwords.c -o cwe-257.o

cwe-257: cwe-257.o syscalls.o crt.o xuartns550/xuartns550.a
	$(GCC) $(INCS) -o cwe-257 cwe-257.o syscalls.o crt.o -Wl,-T,test.ld $(LDOPTS) xuartns550/xuartns550.a

cwe-257.enc: cwe-257.o syscalls.o crt.o uart/uart.a
	$(GCC) $(INCS) -o cwe-257.enc cwe-257.o syscalls.o crt.o -Wl,-T,encrypt.ld $(LDOPTS) uart/uart.a
	$(ENCRYPTOR) -e cwe-257.enc

%.o: %.c
	$(CC) $(INCS) $(CCOPTS) -c $^

%.dump: %.elf
	$(OBJDUMP) --source --disassemble-all --disassemble-zeroes $< > $@

%.sim_dump: %.sim_elf
	$(OBJDUMP) --source --disassemble-all --disassemble-zeroes $< > $@

.SECONDARY:

%: %.o
	$(GCC) $(INCS) -o $@ $^ -Wl,-T,test.ld $(LDOPTS)

%.elf: %.o syscalls.o crt.o xuartns550/xuartns550.a
	$(GCC) $(INCS) -o $@ $^ -Wl,-T,test.ld $(LDOPTS)

%.enc: %.o syscalls.o crt.o xuartns550/xuartns550.a
	$(GCC) $(INCS) -o $@ $^ -Wl,-T,test.ld $(LDOPTS)
	$(ENCRYPTOR) -e $@

%.sim_elf: %.o syscalls.o crt.o uart/uart.a
	$(GCC) $(INCS) -o $@ $^ -Wl,-T,test.ld $(LDOPTS)

%.sim_enc: %.o syscalls.o crt.o uart/uart.a
	$(GCC) $(INCS) -o $@ $^ -Wl,-T,test.ld $(LDOPTS)
	$(ENCRYPTOR) -e $@

clean:
	rm -f $(ATTACKS) *.o *.log *.dump *.out *.elf *.enc *.sim_elf *.sim_dump

