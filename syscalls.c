// See LICENSE for license details.

#include <stdint.h>
#include <string.h>
#include <stdarg.h>
#include <stdio.h>
#include <limits.h>
#include <sys/signal.h>
#include <stdarg.h> //LMB:: ADDED FOR COREMARK
#include "util.h"
#ifdef OUTPUT_uart
#include "uart/uart.h"
#endif /* OUTPUT_uart */

#if !defined(OUTPUT_magic) && !defined(OUTPUT_uart)
#error Either OUTPUT_magic or OUTPUT_uart must be defined, but not both.
#endif

#if defined(OUTPUT_magic) && defined(OUTPUT_uart)
#error Either OUTPUT_magic or OUTPUT_uart must be defined, but not both.
#endif

#define SYS_write 64
#undef strcmp

#ifndef TRUE
#define TRUE	1
#endif /* TRUE */

#ifndef FALSE
#define FALSE	0
#endif /* FALSE */

extern volatile uint64_t tohost;
extern volatile uint64_t fromhost;

static void write_console(char *s, int len)
{
#ifdef OUTPUT_magic
  volatile uint64_t magic_mem[8] __attribute__((aligned(64)));
  magic_mem[0] = SYS_write;
  magic_mem[1] = 1;
  magic_mem[2] = dec_ptr(s);
  magic_mem[3] = (len);
  __sync_synchronize();

  tohost = dec_ptr(magic_mem);
  while (fromhost == 0)
    ;
  fromhost = 0;

  __sync_synchronize();
#endif /* OUTPUT_magic */
#ifdef OUTPUT_uart
  int i;

  for (i=0; i<len; i++)
    {
      uart0_txchar(s[i]);
    }
#endif /* OUTPUT_uart */
}

#define NUM_COUNTERS 2
static uintptr_t counters[NUM_COUNTERS];
static char* counter_names[NUM_COUNTERS];

void setStats(int enable)
{
  int i = 0;
#define READ_CTR(name) do { \
    while (i >= NUM_COUNTERS) ; \
    uintptr_t csr = read_csr(name); \
    if (!enable) { csr -= counters[i]; counter_names[i] = #name; } \
    counters[i++] = csr; \
  } while (0)

  READ_CTR(mcycle);
  READ_CTR(minstret);

#undef READ_CTR
}

void __attribute__((noreturn)) tohost_exit(uintptr_t code)
{
  tohost = (code << 1) | 1;
  while (1);
}

uintptr_t __attribute__((weak)) handle_trap(uintptr_t cause, uintptr_t epc, uintptr_t regs[32])
{
  tohost_exit(1337);
}

void exit(int code)
{
  tohost_exit(code);
}

void abort()
{
  exit(128 + SIGABRT);
}

void printstr(char* s)
{
  write_console(s, strlen(s));
}

void __attribute__((weak)) thread_entry(int cid, int nc)
{
  // multi-threaded programs override this function.
  // for the case of single-threaded programs, only let core 0 proceed.
  while (cid != 0);
}

register void* thread_pointer asm("tp");

static void init_tls()
{
  extern char _tdata_begin, _tdata_end, _tbss_end;
  size_t tdata_size = dec_ptr(&_tdata_end) - dec_ptr(&_tdata_begin);
  memcpy(thread_pointer, &_tdata_begin, tdata_size);
  size_t tbss_size = dec_ptr(&_tbss_end) - dec_ptr(&_tdata_end);
  memset(thread_pointer + tdata_size, 0, tbss_size);
}

int main(int, char**);
void malloc_init(void);

void _init(int cid, int nc)
{
  init_tls();
  thread_entry(cid, nc);

#ifdef OUTPUT_uart
  // initialize the UART for console output
  uart0_init();
#endif /* OUTPUT_uart */

  // initialize the storage allocator
  malloc_init();

  // only single-threaded programs should ever get here.
  int ret = main(0, 0);

#ifdef notdef
  char buf[NUM_COUNTERS * 32] __attribute__((aligned(64)));
  char* pbuf = buf;
  for (int i = 0; i < NUM_COUNTERS; i++)
    if (counters[i])
      pbuf += sprintf(pbuf, "%s = %d\n", counter_names[i], counters[i]);
  if (pbuf != buf)
    printstr(buf);
#endif /* notdef */

  exit(ret);
}

#undef putchar
int putchar(int ch)
{
  static __thread char buf[64] __attribute__((aligned(64)));
  static __thread int buflen = 0;

  buf[buflen++] = ch;

  if (ch == '\n' || buflen == sizeof(buf))
  {
    write_console(buf, buflen);
    buflen = 0;
  }

  return 0;
}

void printhex(uint64_t x)
{
  char str[17];
  int i;
  for (i = 0; i < 16; i++)
  {
    str[15-i] = (x & 0xF) + ((x & 0xF) < 10 ? '0' : 'a'-10);
    x >>= 4;
  }
  str[16] = 0;

  printstr(str);
}

static inline void printnum(void (*putch)(int, void**), void **putdat,
                    unsigned long long num, unsigned base, int width, int padc)
{
  unsigned digs[sizeof(num)*CHAR_BIT];
  int pos = 0;

  while (1)
  {
    digs[pos++] = num % base;
    if (num < base)
      break;
    num /= base;
  }

  while (width-- > pos)
    putch(padc, putdat);

  while (pos-- > 0)
    putch(digs[pos] + (digs[pos] >= 10 ? 'a' - 10 : '0'), putdat);
}

static unsigned long long getuint(va_list *ap, int lflag)
{
  if (lflag >= 2)
    return va_arg(*ap, unsigned long long);
  else if (lflag)
    return va_arg(*ap, unsigned long);
  else
    return va_arg(*ap, unsigned int);
}

static long long getint(va_list *ap, int lflag)
{
  if (lflag >= 2)
    return va_arg(*ap, long long);
  else if (lflag)
    return va_arg(*ap, long);
  else
    return va_arg(*ap, int);
}
/*
static void vprintfmt(void (*putch)(int, void**), void **putdat, const char *fmt, va_list ap)
{
  register const char* p;
  const char* last_fmt;
  register int ch, err;
  unsigned long long num;
  int base, lflag, width, precision, altflag;
  char padc;

  while (1) {
    while ((ch = *(unsigned char *) fmt) != '%') {
      if (ch == '\0')
        return;
      fmt++;
      putch(ch, putdat);
    }
    fmt++;

    // Process a %-escape sequence
    last_fmt = fmt;
    padc = ' ';
    width = -1;
    precision = -1;
    lflag = 0;
    altflag = 0;
  reswitch:
    switch (ch = *(unsigned char *) fmt++) {

    // flag to pad on the right
    case '-':
      padc = '-';
      goto reswitch;
      
    // flag to pad with 0's instead of spaces
    case '0':
      padc = '0';
      goto reswitch;

    // width field
    case '1':
    case '2':
    case '3':
    case '4':
    case '5':
    case '6':
    case '7':
    case '8':
    case '9':
      for (precision = 0; ; ++fmt) {
        precision = precision * 10 + ch - '0';
        ch = *fmt;
        if (ch < '0' || ch > '9')
          break;
      }
      goto process_precision;

    case '*':
      precision = va_arg(ap, int);
      goto process_precision;

    case '.':
      if (width < 0)
        width = 0;
      goto reswitch;

    case '#':
      altflag = 1;
      goto reswitch;

    process_precision:
      if (width < 0)
        width = precision, precision = -1;
      goto reswitch;

    // long flag (doubled for long long)
    case 'l':
      lflag++;
      goto reswitch;

    // character
    case 'c':
      putch(va_arg(ap, int), putdat);
      break;

    // string
    case 's':
      if ((p = va_arg(ap, char *)) == NULL)
        p = "(null)";
      if (width > 0 && padc != '-')
        for (width -= strnlen(p, precision); width > 0; width--)
          putch(padc, putdat);
      for (; (ch = *p) != '\0' && (precision < 0 || --precision >= 0); width--) {
        putch(ch, putdat);
        p++;
      }
      for (; width > 0; width--)
        putch(' ', putdat);
      break;

    // (signed) decimal
    case 'd':
      num = getint(&ap, lflag);
      if ((long long) num < 0) {
        putch('-', putdat);
        num = -(long long) num;
      }
      base = 10;
      goto signed_number;

    // unsigned decimal
    case 'u':
      base = 10;
      goto unsigned_number;

    // (unsigned) octal
    case 'o':
      // should do something with padding so it's always 3 octits
      base = 8;
      goto unsigned_number;

    // pointer
    case 'p':
      static_assert(sizeof(long) == sizeof(void*));
      lflag = 1;
      putch('0', putdat);
      putch('x', putdat);
      // fall through to 'x'

    // (unsigned) hexadecimal
    case 'x':
      base = 16;
    unsigned_number:
      num = getuint(&ap, lflag);
    signed_number:
      printnum(putch, putdat, num, base, width, padc);
      break;

    // escaped '%' character
    case '%':
      putch(ch, putdat);
      break;
      
    // unrecognized escape sequence - just print it literally
    default:
      putch('%', putdat);
      fmt = last_fmt;
      break;
    }
  }
}



int printf(const char* fmt, ...)
{
  va_list ap;
  va_start(ap, fmt);

  vprintfmt((void*)putchar, 0, fmt, ap);

  va_end(ap);
  return 0; // incorrect return value, but who cares, anyway?
}

void sprintf_putch(int ch, void** data)
{
  char** pstr = (char**)data;
  **pstr = ch;
  (*pstr)++;
}

int sprintf(char* str, const char* fmt, ...)
{
  va_list ap;
  char* str0 = str;
  va_start(ap, fmt);


  vprintfmt(sprintf_putch, (void**)&str, fmt, ap);
  *str = 0;

  va_end(ap);
  return str - str0;
}
*/
void* memcpy(void* dest, const void* src, size_t len)
{
  if ((((uintptr_t)dest | (uintptr_t)dec_ptr(src) | len) & (sizeof(uintptr_t)-1)) == 0) {
    const uintptr_t* s = src;
    uintptr_t *d = dest;
    while (d < (uintptr_t*)(dest + len))
      *d++ = *s++;
  } else {
    const char* s = src;
    char *d = dest;
    while (d < (char*)(dest + len))
      *d++ = *s++;
  }
  return dest;
}

void* memset(void* dest, int byte, size_t len)
{
  if ((((uintptr_t)dest | len) & (sizeof(uintptr_t)-1)) == 0) {
    uintptr_t word = byte & 0xFF;
    word |= word << 8;
    word |= word << 16;
    word |= word << 16 << 16;

    uintptr_t *d = dest;
    while (d < (uintptr_t*)(dest + len))
      *d++ = word;
  } else {
    char *d = dest;
    while (d < (char*)(dest + len))
      *d++ = byte;
  }
  return dest;
}

size_t strlen(const char *s)
{
  // const char *p = s;
  // while (*p)
  //   p++;
  // return dec_ptr(p) - dec_ptr(s);
  size_t i = 0;
  while (s[i]) {
    i++;
  }
  return i;
}

size_t strnlen(const char *s, size_t n)
{
  const char *p = s;
  // while (n-- && *p)
  //   p++;
  // return dec_ptr(p) - dec_ptr(s);
  size_t i = 0;
  while (n-- && p[i]) {
    i++;
  }
  return i;
}

int strcmp(const char* s1, const char* s2)
{
  unsigned char c1, c2;

  do {
    c1 = *s1++;
    c2 = *s2++;
  } while (c1 != 0 && c1 == c2);

  return dec_ptr(c1) - dec_ptr(c2);
}

char* strcpy(char* dest, const char* src)
{
  char* d = dest;
  while ((*d++ = *src++))
    ;
  return dest;
}

long atol(const char* str)
{
  long res = 0;
  int sign = 0;

  while (*str == ' ')
    str++;

  if (*str == '-' || *str == '+') {
    sign = *str == '-';
    str++;
  }

  while (*str) {
    res *= 10;
    res += *str++ - '0';
  }

  return sign ? -res : res;
}

/* rand() implementation from FreeBSD */
static int
do_rand(unsigned long *ctx)
{
/*
 * Compute x = (7^5 * x) mod (2^31 - 1)
 * without overflowing 31 bits:
 *      (2^31 - 1) = 127773 * (7^5) + 2836
 * From "Random number generators: good ones are hard to find",
 * Park and Miller, Communications of the ACM, vol. 31, no. 10,
 * October 1988, p. 1195.
 */
  long hi, lo, x;

  /* Must be in [1, 0x7ffffffe] range at this point. */
  hi = *ctx / 127773;
  lo = *ctx % 127773;
  x = 16807 * lo - 2836 * hi;
  if (x < 0)
    x += 0x7fffffff;
  *ctx = x;
  /* Transform to [0, 0x7ffffffd] range. */
  return (x - 1);
}

int
rand_r(unsigned int *ctx)
{
  unsigned long val;
  int r;

  /* Transform to [1, 0x7ffffffe] range. */
  val = (*ctx % 0x7ffffffe) + 1;
  r = do_rand(&val);

  *ctx = (unsigned int)(val - 1);
  return (r);
}

static unsigned long next = 2;

int
rand(void)
{
  return (do_rand(&next));
}

void
srand(unsigned int seed)
{
  next = seed;
  /* Transform to [1, 0x7ffffffe] range. */
  next = (next % 0x7ffffffe) + 1;
}

int
isupper(int c)
{
  return (c >= 'A' && c <= 'Z');
}

int tolower(int c)
{
  if (c >= 65 && c <= 90)
    c += 32;
  return (c);
}

char *strchr(const char *s, int c)
{
    while (s[0] != c && s[0] != '\0')
        s++;
    if (s[0] == '\0')
        return NULL;
    else
        return (char *)s;
}

#define IN(l,a,r) (((l) <= (a)) && ((a) <= (r)))

/*
 * This is a simple implementation of Standard C strtol().  A library
 * version should be programmed with more care.
 */
long
strtol(const char  *nptr, /*@null@*/ char** endptr, int base)
{
    int			c;		/* current character value */
    int			digit;		/* digit value */
    static const char   *digits = "0123456789abcdefghijklmnopqrstuvxwyz";
    bool		is_negative;	/* false for positive, true for negative */
    long		number;		/* the accumulating number */
    const char		*pos;		/* pointer into digit list */
    const char		*q;		/* pointer past end of digits */

    if (!(IN(2,base,36) || (base == 0) || (nptr != (const char*)NULL)))
    {
	if (endptr != (char**)NULL)
	    *endptr = (char*)nptr;
	return (0L);
    }

    while (*nptr == ' ' || *nptr == '\t')
	nptr++;				/* ignore leading whitespace */

    switch (*nptr)			/* set number sign */
    {
    case '-':
	is_negative = true;
	nptr++;
	break;

    case '+':
	is_negative = false;
	nptr++;
	break;

    default:
	is_negative = false;
	break;
    }

    q = nptr;
    if (base == 0)			/* variable base; set by lookahead */
    {
	if (*q == '0')
	    base = ((*(q+1) == 'x') || (*(q+1) == 'X')) ? 16 : 8;
	else
	    base = 10;
    }

    /* eliminate optional "0x" or "0X" prefix */
    if ((base == 16) &&
	(*q == '0') &&
	((*(q+1) == 'x') || (*(q+1) == 'X')) )
	q += 2;

    number = 0L;

    /* Number conversion is done by shifting rather than multiplication
       when the base is a power of 2, in order that the results not be
       impacted by integer overflow. */
    switch (base)
    {
    case 2:
	while (IN('0',*q,'1'))
	{
	    number <<= 1;
	    number |= *q - '0';
	    q++;
	}
	break;

    case 16:
	for (;;)
	{
	    if (*q == '\0')
		break;
	    c = (int)(unsigned)*q;
	    if (isupper(c))
		c = tolower(c);
	    pos = strchr(digits,c);
	    if (pos == (char*)NULL)
		break;
	    digit = (int)(pos - digits);
	    if (!IN(0,digit,15))
		break;
	    number <<= 4;
	    number |= digit;
	    q++;
	}
	break;


    default:		/* all other bases done by multiplication */
	for (;;)	/* accumulate negative so most negative */
	{		/* number on two's-complement is handled */
	    if (*q == '\0')
		break;
	    c = (int)(unsigned)*q;
	    if (isupper(c))
		c = tolower(c);
	    pos = strchr(digits,c);
	    if (pos == (char*)NULL)
		break;
	    digit = (int)(pos - digits);
	    if (!IN(0,digit,base-1))
		break;
	    number *= base;
	    number -= digit;
	    q++;
	}
	if (endptr != (char**)NULL)
	    *endptr = (char*)q;
	if (is_negative)
	    return(number);
	number = -number;
	break;
    }
    if (is_negative)
	number = -number;
    if (endptr != (char**)NULL)
	*endptr = (char*)q;
    return (number);
}

//
// jank-alicious malloc/free implementation
//
#define MAXPOOL		16
#define MAXALLOC	128

unsigned char malloc_pool[MAXPOOL][MAXALLOC];
unsigned char malloc_busy[MAXPOOL];

void
malloc_init(void)
{
  for (int i=0; i < MAXPOOL; i++){
    malloc_busy[i] = FALSE;
    memset(malloc_pool[i], '\0', MAXALLOC);
  }
}

void *
malloc (unsigned int size)
{
  if (size > MAXALLOC)
    return NULL;

  for (int i=0; i < MAXPOOL; i++)
  {
    if (!malloc_busy[i])
    {
      malloc_busy[i] = TRUE;
      return malloc_pool[i];
    }
  }

  // no storage available, if we get here
  return NULL;
}

void
free(void *p)
{
  for (int i=0; i < MAXPOOL; i++)
  {
    if (p == malloc_pool[i])
    {
      malloc_busy[i] = FALSE;
      return;
    }
  }

  // invalid free if we get here
  exit(1);
}



//COREMARK EE_PRINTF IMPL
#define ZEROPAD  	(1<<0)	/* Pad with zero */
#define SIGN    	(1<<1)	/* Unsigned/signed long */
#define PLUS    	(1<<2)	/* Show plus */
#define SPACE   	(1<<3)	/* Spacer */
#define LEFT    	(1<<4)	/* Left justified */
#define HEX_PREP 	(1<<5)	/* 0x */
#define UPPERCASE   (1<<6)	/* 'ABCDEF' */

#define is_digit(c) ((c) >= '0' && (c) <= '9')

static char *digits = "0123456789abcdefghijklmnopqrstuvwxyz";
static char *upper_digits = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
/*static size_t strnlen(const char *s, size_t count);

static size_t strnlen(const char *s, size_t count)
{
  const char *sc;
  for (sc = s; *sc != '\0' && count--; ++sc);
  return sc - s;
}*/

static int skip_atoi(const char **s)
{
  int i = 0;
  while (is_digit(**s)) i = i*10 + *((*s)++) - '0';
  return i;
}

/*
void* memset(void* dest, int byte, size_t len)
{
    if ((((uintptr_t)dest | len) & (sizeof(uintptr_t)-1)) == 0) {
        uintptr_t word = byte & 0xFF;
        word |= word << 8;
        word |= word << 16;
        word |= word << 16 << 16;

        uintptr_t *d = dest;
        while (d < (uintptr_t*)(dest + len))
            *d++ = word;
    } else {
        char *d = dest;
        while (d < (char*)(dest + len))
            *d++ = byte;
    }
    return dest;
}*/

long long int char_written;

static char *number(char *str, long num, int base, int size, int precision, int type)
{
  char c, sign, tmp[66];
  char *dig = digits;
  int i;

  if (type & UPPERCASE)  dig = upper_digits;
  if (type & LEFT) type &= ~ZEROPAD;
  if (base < 2 || base > 36) return 0;
  
  c = (type & ZEROPAD) ? '0' : ' ';
  sign = 0;
  if (type & SIGN)
  {
    if (num < 0)
    {
      sign = '-';
      num = -num;
      size--;
    }
    else if (type & PLUS)
    {
      sign = '+';
      size--;
    }
    else if (type & SPACE)
    {
      sign = ' ';
      size--;
    }
  }

  if (type & HEX_PREP)
  {
    if (base == 16)
      size -= 2;
    else if (base == 8)
      size--;
  }

  i = 0;

  if (num == 0)
    tmp[i++] = '0';
  else
  {
    while (num != 0)
    {
      tmp[i++] = dig[((unsigned long) num) % (unsigned) base];
      num = ((unsigned long) num) / (unsigned) base;
    }
  }

  if (i > precision) precision = i;
  size -= precision;
  if (!(type & (ZEROPAD | LEFT))) while (size-- > 0) { *str++ = ' '; char_written++; }
  if (sign) { *str++ = sign; char_written++; }
  
  if (type & HEX_PREP)
  {
    if (base == 8) {
      *str++ = '0';
      char_written++;
    }
    else if (base == 16)
    {
      *str++ = '0';
      *str++ = digits[33];
      char_written += 2;
    }
  }

  if (!(type & LEFT)) while (size-- > 0) { *str++ = c; char_written++;}
  while (i < precision--) { *str++ = '0'; char_written++; }
  while (i-- > 0) { *str++ = tmp[i]; char_written++; }
  while (size-- > 0) { *str++ = ' '; char_written++; }

  return str;
}

static char *eaddr(char *str, unsigned char *addr, int size, int precision, int type)
{
  char tmp[24];
  char *dig = digits;
  int i, len;

  if (type & UPPERCASE)  dig = upper_digits;
  len = 0;
  for (i = 0; i < 6; i++)
  {
    if (i != 0) tmp[len++] = ':';
    tmp[len++] = dig[addr[i] >> 4];
    tmp[len++] = dig[addr[i] & 0x0F];
  }

  if (!(type & LEFT)) while (len < size--) { *str++ = ' '; char_written++; }
  for (i = 0; i < len; ++i) { *str++ = tmp[i]; char_written++; }
  while (len < size--) { *str++ = ' '; char_written++; }

  return str;
}

static char *iaddr(char *str, unsigned char *addr, int size, int precision, int type)
{
  char tmp[24];
  int i, n, len;

  len = 0;
  for (i = 0; i < 4; i++)
  {
    if (i != 0) tmp[len++] = '.';
    n = addr[i];
    
    if (n == 0)
      tmp[len++] = digits[0];
    else
    {
      if (n >= 100) 
      {
        tmp[len++] = digits[n / 100];
        n = n % 100;
        tmp[len++] = digits[n / 10];
        n = n % 10;
      }
      else if (n >= 10) 
      {
        tmp[len++] = digits[n / 10];
        n = n % 10;
      }

      tmp[len++] = digits[n];
    }
  }

  if (!(type & LEFT)) while (len < size--) { *str++ = ' '; char_written++; }
  for (i = 0; i < len; ++i) { *str++ = tmp[i]; char_written++; }
  while (len < size--) { *str++ = ' '; char_written++; }

  return str;
}

#if HAS_FLOAT

char *ecvtbuf(double arg, int ndigits, int *decpt, int *sign, char *buf);
char *fcvtbuf(double arg, int ndigits, int *decpt, int *sign, char *buf);
static void ee_bufcpy(char *d, char *s, int count); 
 
void ee_bufcpy(char *pd, char *ps, int count) {
	char *pe=ps+count;
	while (ps!=pe)
		*pd++=*ps++;
}

static void parse_float(double value, char *buffer, char fmt, int precision)
{
  int decpt, sign, exp, pos;
  char *digits = enc_ptr(NULL);
  char cvtbuf[80];
  int capexp = 0;
  int magnitude;

  if (fmt == 'G' || fmt == 'E')
  {
    capexp = 1;
    fmt += 'a' - 'A';
  }

  if (fmt == 'g')
  {
    digits = ecvtbuf(value, precision, &decpt, &sign, cvtbuf);
    magnitude = decpt - 1;
    if (magnitude < -4  ||  magnitude > precision - 1)
    {
      fmt = 'e';
      precision -= 1;
    }
    else
    {
      fmt = 'f';
      precision -= decpt;
    }
  }

  if (fmt == 'e')
  {
    digits = ecvtbuf(value, precision + 1, &decpt, &sign, cvtbuf);

    if (sign) *buffer++ = '-';
    *buffer++ = *digits;
    if (precision > 0) *buffer++ = '.';
    ee_bufcpy(buffer, digits + 1, precision);
    buffer += precision;
    *buffer++ = capexp ? 'E' : 'e';

    if (decpt == 0)
    {
      if (value == 0.0)
        exp = 0;
      else
        exp = -1;
    }
    else
      exp = decpt - 1;

    if (exp < 0)
    {
      *buffer++ = '-';
      exp = -exp;
    }
    else
      *buffer++ = '+';

    buffer[2] = (exp % 10) + '0';
    exp = exp / 10;
    buffer[1] = (exp % 10) + '0';
    exp = exp / 10;
    buffer[0] = (exp % 10) + '0';
    buffer += 3;
  }
  else if (fmt == 'f')
  {
    digits = fcvtbuf(value, precision, &decpt, &sign, cvtbuf);
    if (sign) *buffer++ = '-';
    if (*digits)
    {
      if (decpt <= 0)
      {
        *buffer++ = '0';
        *buffer++ = '.';
        for (pos = 0; pos < -decpt; pos++) *buffer++ = '0';
        while (*digits) *buffer++ = *digits++;
      }
      else
      {
        pos = 0;
        while (*digits)
        {
          if (pos++ == decpt) *buffer++ = '.';
          *buffer++ = *digits++;
        }
      }
    }
    else
    {
      *buffer++ = '0';
      if (precision > 0)
      {
        *buffer++ = '.';
        for (pos = 0; pos < precision; pos++) *buffer++ = '0';
      }
    }
  }

  *buffer = '\0';
}

static void decimal_point(char *buffer)
{
  while (*buffer)
  {
    if (*buffer == '.') return;
    if (*buffer == 'e' || *buffer == 'E') break;
    buffer++;
  }

  if (*buffer)
  {
    int n = strnlen(buffer,256);
    while (n > 0) 
    {
      buffer[n + 1] = buffer[n];
      n--;
    }

    *buffer = '.';
  }
  else
  {
    *buffer++ = '.';
    *buffer = '\0';
  }
}

static void cropzeros(char *buffer)
{
  char *stop;

  while (*buffer && *buffer != '.') buffer++;
  if (*buffer++)
  {
    while (*buffer && *buffer != 'e' && *buffer != 'E') buffer++;
    stop = buffer--;
    while (*buffer == '0') buffer--;
    if (*buffer == '.') buffer--;
    while (buffer!=stop)
		*++buffer=0;
  }
}

static char *flt(char *str, double num, int size, int precision, char fmt, int flags)
{
  char tmp[80];
  char c, sign;
  int n, i;

  // Left align means no zero padding
  if (flags & LEFT) flags &= ~ZEROPAD;

  // Determine padding and sign char
  c = (flags & ZEROPAD) ? '0' : ' ';
  sign = 0;
  if (flags & SIGN)
  {
    if (num < 0.0)
    {
      sign = '-';
      num = -num;
      size--;
    }
    else if (flags & PLUS)
    {
      sign = '+';
      size--;
    }
    else if (flags & SPACE)
    {
      sign = ' ';
      size--;
    }
  }

  // Compute the precision value
  if (precision < 0)
    precision = 6; // Default precision: 6

  // Convert floating point number to text
  parse_float(num, tmp, fmt, precision);

  if ((flags & HEX_PREP) && precision == 0) decimal_point(tmp);
  if (fmt == 'g' && !(flags & HEX_PREP)) cropzeros(tmp);

  n = strnlen(tmp,256);

  // Output number with alignment and padding
  size -= n;
  if (!(flags & (ZEROPAD | LEFT))) while (size-- > 0) { *str++ = ' '; char_written++; }
  if (sign) { *str++ = sign; char_written++; }
  if (!(flags & LEFT)) while (size-- > 0) { *str++ = c; char_written++; }
  for (i = 0; i < n; i++) { *str++ = tmp[i]; char_written++; }
  while (size-- > 0) { *str++ = ' '; char_written++; }

  return str;
}

#endif

static int ee_vsprintf(char *buf, const char *fmt, va_list args)
{
  int len;
  unsigned long num;
  int i, base;
  char *str;
  char *s;

  char_written = 0;
  int flags;            // Flags to number()

  int field_width;      // Width of output field
  int precision;        // Min. # of digits for integers; max number of chars for from string
  int qualifier;        // 'h', 'l', or 'L' for integer fields

  for (str = buf; *fmt; fmt++)
  {
    if (*fmt != '%')
    {
      *str++ = *fmt;
      char_written++;
      continue;
    }
                  
    // Process flags
    flags = 0;
repeat:
    fmt++; // This also skips first '%'
    switch (*fmt)
    {
      case '-': flags |= LEFT; goto repeat;
      case '+': flags |= PLUS; goto repeat;
      case ' ': flags |= SPACE; goto repeat;
      case '#': flags |= HEX_PREP; goto repeat;
      case '0': flags |= ZEROPAD; goto repeat;
    }
          
    // Get field width
    field_width = -1;
    if (is_digit(*fmt))
      field_width = skip_atoi(&fmt);
    else if (*fmt == '*')
    {
      fmt++;
      field_width = va_arg(args, int);
      if (field_width < 0)
      {
        field_width = -field_width;
        flags |= LEFT;
      }
    }

    // Get the precision
    precision = -1;
    if (*fmt == '.')
    {
      ++fmt;    
      if (is_digit(*fmt))
        precision = skip_atoi(&fmt);
      else if (*fmt == '*')
      {
        ++fmt;
        precision = va_arg(args, int);
      }
      if (precision < 0) precision = 0;
    }

    // Get the conversion qualifier
    qualifier = -1;
    if (*fmt == 'l' || *fmt == 'L')
    {
      qualifier = *fmt;
      fmt++;
    }

    // Default base
    base = 10;

    switch (*fmt)
    {
      case 'c':
        if (!(flags & LEFT)) while (--field_width > 0) { *str++ = ' '; char_written++; }
        *str++ = (unsigned char) va_arg(args, int);
	char_written++;
        while (--field_width > 0) { *str++ = ' '; char_written++; }
        continue;

      case 's':
        s = va_arg(args, char *);
        if (!s) s = "<NULL>";
        len = strnlen(s, precision);
        if (!(flags & LEFT)) while (len < field_width--) { *str++ = ' '; char_written++; }
        for (i = 0; i < len; ++i) { *str++ = *s++; char_written++; }
        while (len < field_width--) { *str++ = ' '; char_written++; }
        continue;

      case 'p':
        if (field_width == -1)
        {
          field_width = 2 * sizeof(void *);
          flags |= ZEROPAD;
        }
        str = number(str, (unsigned long) va_arg(args, void *), 16, field_width, precision, flags);
        continue;

      case 'A':
        flags |= UPPERCASE;

      case 'a':
        if (qualifier == 'l')
          str = eaddr(str, va_arg(args, unsigned char *), field_width, precision, flags);
        else
          str = iaddr(str, va_arg(args, unsigned char *), field_width, precision, flags);
        continue;

      // Integer number formats - set up the flags and "break"
      case 'o':
        base = 8;
        break;

      case 'X':
        flags |= UPPERCASE;

      case 'x':
        base = 16;
        break;

      case 'd':
      case 'i':
        flags |= SIGN;

      case 'u':
        break;

#if HAS_FLOAT

      case 'f':
        str = flt(str, va_arg(args, double), field_width, precision, *fmt, flags | SIGN);
        continue;

#endif

      default:
        if (*fmt != '%') { *str++ = '%'; char_written++; }
        if (*fmt) { *str++ = *fmt; char_written++; }
        else
          --fmt; //Not sure when this case happens
        continue;
    }

    if (qualifier == 'l')
      num = va_arg(args, unsigned long);
    else if (flags & SIGN)
      num = va_arg(args, int);
    else
      num = va_arg(args, unsigned int);

    str = number(str, num, base, field_width, precision, flags);
  }

  *str = '\0';
  return char_written;
}

int printf(const char *fmt, ...)
{
  char buf[256],*p;
  va_list args;
  int n=0;

  va_start(args, fmt);
  ee_vsprintf(buf, fmt, args);
  va_end(args);
  p=buf;
  while (*p) {
    uart0_txchar(*p);
    n++;
    p++;
  }

  return n;
}

